package org.rhyv.testapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class AddEditItemActivity extends AppCompatActivity {
  public static final String EXTRA_ID           = "org.rhyv.testapp.ID";
  public static final String EXTRA_NAME         = "org.rhyv.testapp.NAME";
  public static final String EXTRA_DESCRIPTION  = "org.rhyv.testapp.DESCRIPTION";
  public static final String EXTRA_PRICE        = "org.rhyv.testapp.PRICE";
  private EditText etName;
  private EditText etDescription;
  private EditText etPrice;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_edit_item);

    etName = findViewById( R.id.etName );
    etDescription = findViewById( R.id.etDescription );
    etPrice = findViewById( R.id.etPrice );

    getSupportActionBar().setHomeAsUpIndicator( R.drawable.ic_close_24 );

    Intent intent = getIntent();

    if( intent.hasExtra( EXTRA_ID )){
      setTitle( "Edit Item" );
      etName.setText( intent.getStringExtra( EXTRA_NAME ));
      etDescription.setText( intent.getStringExtra( EXTRA_DESCRIPTION ));
      etPrice.setText( intent.getStringExtra( EXTRA_PRICE ));
    } else {
      setTitle( "Add Item" );
    }
  }

  @Override
  public boolean onCreateOptionsMenu( Menu menu ){
    MenuInflater inflater = getMenuInflater();
    inflater.inflate( R.menu.add_item_menu, menu );
    return true;
  }

  @Override
  public boolean onOptionsItemSelected( @NonNull MenuItem item ){
    switch( item.getItemId()){
      case R.id.save_item:
        saveItem();
        return true;
      default:
        return super.onOptionsItemSelected( item );
    }
  }

  private void saveItem(){
    String name         = etName.getText().toString();
    String description  = etDescription.getText().toString();
    String price        = etPrice.getText().toString();

    if( name.trim().isEmpty() || description.trim().isEmpty() || price.trim().isEmpty()){
      Toast.makeText( this, "Please fill all fields", Toast.LENGTH_SHORT ).show();
      return;
    }

    Intent data = new Intent();

    int id = getIntent().getIntExtra( EXTRA_ID, -1 );
    if( id != -1 )
      data.putExtra( EXTRA_ID, id );
    data.putExtra( EXTRA_NAME, name );
    data.putExtra( EXTRA_DESCRIPTION, description );
    data.putExtra( EXTRA_PRICE, price );

    setResult( RESULT_OK, data );
    finish();
  }
}