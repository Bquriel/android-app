package org.rhyv.testapp.databases;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import org.rhyv.testapp.databases.daos.ItemDao;
import org.rhyv.testapp.databases.entities.Item;

@Database( entities = { Item.class }, version = 4)
public abstract class AppDatabase extends RoomDatabase {
  public abstract ItemDao itemDao();

  private static AppDatabase instance;

  public static synchronized AppDatabase getInstance( Context ctx ){
    if( instance == null ){
      instance = Room.databaseBuilder( ctx.getApplicationContext(), AppDatabase.class, "app-db" ).fallbackToDestructiveMigration().addCallback( callback ).build();
    }

    return instance;
  };

  private static RoomDatabase.Callback callback = new RoomDatabase.Callback(){
    @Override
    public void onCreate( @NonNull SupportSQLiteDatabase db ){
      super.onCreate( db );
      new PopulateDb( instance ).execute();
    }
  };

  private static class PopulateDb extends AsyncTask< Void, Void, Void >{
    private ItemDao itemDao;

    private PopulateDb( AppDatabase db ){
      itemDao = db.itemDao();
    }

    @Override
    protected Void doInBackground(Void... voids){
      itemDao.insertOne( new Item( "Peach", "Fresh peaches from Estonia", "1€" ));
      itemDao.insertOne( new Item( "Tomato", "Fresh salad tomatoes from Funland", "2.5€" ));
      itemDao.insertOne( new Item( "Squash", "Fresh yellow squash from sweden", "0.8€" ));
      return null;
    }
  };
}