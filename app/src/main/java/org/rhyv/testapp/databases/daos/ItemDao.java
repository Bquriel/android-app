package org.rhyv.testapp.databases.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import org.rhyv.testapp.databases.entities.Item;

import java.util.List;

@Dao
public interface ItemDao {
  @Query( "SELECT * FROM items" )
  LiveData<List<Item>> getAll();

  @Query( "SELECT * FROM items WHERE _id IN (:ids)" )
  List<Item> findByIds( int[] ids );

  @Query( "SELECT * FROM items WHERE name LIKE :name LIMIT 1" )
  Item findByName( String name );

  @Insert
  void insertMany( Item... items );

  @Insert
  void insertOne( Item item );

  @Update
  void updateOne( Item item );

  @Delete
  void delete( Item item );

  @Query( "DELETE FROM items" )
  void deleteAll();
}
