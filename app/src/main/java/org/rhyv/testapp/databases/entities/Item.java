package org.rhyv.testapp.databases.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "items")
public class Item {
  @PrimaryKey(autoGenerate = true)
  public int _id;

  @ColumnInfo(name = "name")
  public String name;

  @ColumnInfo(name = "description")
  public String description;

  @ColumnInfo(name = "price")
  public String price;

  public Item( String name, String description, String price ){
    this.name         = name;
    this.description  = description;
    this.price        = price;
  }
}
