package org.rhyv.testapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import org.rhyv.testapp.R;
import org.rhyv.testapp.databases.entities.Item;

public class ListItemAdapter extends ListAdapter< Item, ListItemAdapter.ViewHolder >{
  LayoutInflater inflater;
  // private List<Item> items = new ArrayList<>();
  private OnListItemClickListener onClickListener;
  private static final DiffUtil.ItemCallback<Item> DIFF_CALLBACK = new DiffUtil.ItemCallback<Item>() {
    @Override
    public boolean areItemsTheSame( @NonNull Item oldItem, @NonNull Item newItem ){
      return oldItem._id == newItem._id;
    }

    @Override
    public boolean areContentsTheSame( @NonNull Item oldItem, @NonNull Item newItem ){
      // TODO: implement compare function in item class.
      return oldItem.name.equals( newItem.name ) && oldItem.description.equals( newItem.description ) && oldItem.price.equals( newItem.price );
    }
  };


  public interface OnListItemClickListener {
    void onListItemClicked( Item item );
  }

  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView tvName;
    public TextView tvDescription;
    public TextView tvPrice;
    OnListItemClickListener listener;

    public ViewHolder( View v, OnListItemClickListener l ){
      super( v );

      this.tvName         = v.findViewById( R.id.tvName );
      this.tvDescription  = v.findViewById( R.id.tvDescription );
      this.tvPrice        = v.findViewById( R.id.tvPrice );
      this.listener = l;
      v.setOnClickListener( this );
    }

    @Override
    public void onClick( View view ){
      int position = getAdapterPosition();
      if( listener != null && position != RecyclerView.NO_POSITION ){
        listener.onListItemClicked( getItem( position ));
      }
    }
  }


  //******************** CLASS CONTENT ********************//
  public ListItemAdapter( Context ctx ){
    super( DIFF_CALLBACK );
    this.inflater = (LayoutInflater) ctx.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
  }

  @Override
  public ListItemAdapter.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType ){
    View v = LayoutInflater.from( parent.getContext()).inflate( R.layout.listview_detail, parent, false );
    return new ViewHolder( v, onClickListener );
  }

  @Override
  public void onBindViewHolder( ViewHolder holder, int i ){
    Item item = getItem( i );

    holder.tvName.setText( item.name );
    holder.tvDescription.setText( item.description );
    holder.tvPrice.setText( item.price );
  }

  public void setOnItemClickListener( OnListItemClickListener listener ){
    this.onClickListener = listener;
  }

  public Item getItemAt( int i ){
    return getItem( i );
  }
}
