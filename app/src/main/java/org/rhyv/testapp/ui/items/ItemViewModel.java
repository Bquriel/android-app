package org.rhyv.testapp.ui.items;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.rhyv.testapp.repositories.ItemRepository;
import org.rhyv.testapp.databases.entities.Item;

import java.util.List;

public class ItemViewModel extends AndroidViewModel {
  private ItemRepository repository;
  private LiveData<List<Item>> items;

  public ItemViewModel( @NonNull Application application ){
    super( application );
    repository  = new ItemRepository( application );
    items       = repository.getItems();
  }

  public LiveData<List<Item>> getItems(){
    return items;
  }

  public void insertOne(Item item ){
    repository.insertOne( item );
  }

  public void updateOne( Item item ){
    repository.updateOne( item );
  }

  public void deleteOne( Item item ){
    repository.deleteOne( item );
  }

  public void deleteAll(){
    repository.deleteAll();
  }
}
