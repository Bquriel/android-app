package org.rhyv.testapp.ui.items;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.rhyv.testapp.AddEditItemActivity;
import org.rhyv.testapp.R;
import org.rhyv.testapp.adapters.ListItemAdapter;
import org.rhyv.testapp.databases.entities.Item;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class ItemFragment extends Fragment {
  public static final int ADD_ITEM_REQUEST    = 1;
  public static final int EDIT_ITEM_REQUEST   = 2;
  public static final String EXTRA_ITEM_INDEX = "org.rhyv.testapp.ITEM_INDEX";
  private RecyclerView rvContent;
  private ItemViewModel itemViewModel;

  public View onCreateView( @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ){
    View root = inflater.inflate( R.layout.fragment_items, container, false );
    FloatingActionButton buttonAdd = root.findViewById( R.id.fabAddItem );
    buttonAdd.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent( getActivity(), AddEditItemActivity.class );
        startActivityForResult( intent, ADD_ITEM_REQUEST );
      }
    });

    rvContent = root.findViewById( R.id.rvContent );
    // use a linear layout manager
    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getActivity());
    rvContent.setLayoutManager(layoutManager);
    rvContent.setHasFixedSize( true );
    final ListItemAdapter adapter = new ListItemAdapter(  getActivity());
    rvContent.setAdapter( adapter );

    itemViewModel = ViewModelProviders.of( this ).get( ItemViewModel.class );
    itemViewModel.getItems().observe( getActivity(), new Observer<List<Item>>() {
      @Override
      public void onChanged( List<Item> items ){
        adapter.submitList( items );
      }
    });
    adapter.setOnItemClickListener(new ListItemAdapter.OnListItemClickListener() {
      @Override
      public void onListItemClicked( Item item ){
        Intent intent = new Intent(  getActivity(), AddEditItemActivity.class );
        intent.putExtra( AddEditItemActivity.EXTRA_ID, item._id );
        intent.putExtra( AddEditItemActivity.EXTRA_NAME, item.name );
        intent.putExtra( AddEditItemActivity.EXTRA_DESCRIPTION, item.description );
        intent.putExtra( AddEditItemActivity.EXTRA_PRICE, item.price );
        startActivityForResult( intent, EDIT_ITEM_REQUEST );
      }
    });

    new ItemTouchHelper(new ItemTouchHelper.SimpleCallback( 0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT ){
      @Override
      public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return false;
      }

      @Override
      public void onSwiped( @NonNull RecyclerView.ViewHolder viewHolder, int direction ){
        itemViewModel.deleteOne( adapter.getItemAt( viewHolder.getAdapterPosition()));
        Toast.makeText(  getActivity(), "Item deleted!", Toast.LENGTH_SHORT ).show();
      }
    }).attachToRecyclerView( rvContent );
    return root;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
    super.onActivityResult( requestCode, resultCode, data );

    if( requestCode == ADD_ITEM_REQUEST && resultCode == RESULT_OK ){
      String name         = data.getStringExtra( AddEditItemActivity.EXTRA_NAME );
      String description  = data.getStringExtra( AddEditItemActivity.EXTRA_DESCRIPTION );
      String price        = data.getStringExtra( AddEditItemActivity.EXTRA_PRICE );

      Item item = new Item( name, description, price );
      itemViewModel.insertOne( item );

      Toast.makeText( getActivity(), "Item saved!", Toast.LENGTH_SHORT ).show();
    } else if( requestCode == EDIT_ITEM_REQUEST && resultCode == RESULT_OK ){
      int id = data.getIntExtra( AddEditItemActivity.EXTRA_ID, -1 );
      if( id == -1 ){
        Toast.makeText( getActivity(), "Error in updating item!", Toast.LENGTH_SHORT ).show();
        return;
      }
      String name         = data.getStringExtra( AddEditItemActivity.EXTRA_NAME );
      String description  = data.getStringExtra( AddEditItemActivity.EXTRA_DESCRIPTION );
      String price        = data.getStringExtra( AddEditItemActivity.EXTRA_PRICE );

      Item item = new Item( name, description, price );
      item._id = id;
      itemViewModel.updateOne( item );
      Toast.makeText( getActivity(), "Item updated!", Toast.LENGTH_SHORT ).show();
    } else {
      Toast.makeText( getActivity(), "Item not saved!", Toast.LENGTH_SHORT ).show();
    }
  }

  @Override
  public boolean onOptionsItemSelected( @NonNull MenuItem item ){
    switch( item.getItemId()){
      case R.id.delete_all_items:
        itemViewModel.deleteAll();
        Toast.makeText( getActivity(), "All items deleted!", Toast.LENGTH_SHORT ).show();
      default:
        return super.onOptionsItemSelected( item );
    }
  }
}