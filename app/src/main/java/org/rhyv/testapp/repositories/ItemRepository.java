package org.rhyv.testapp.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import org.rhyv.testapp.databases.AppDatabase;
import org.rhyv.testapp.databases.daos.ItemDao;
import org.rhyv.testapp.databases.entities.Item;

import java.util.List;

public class ItemRepository {
  private ItemDao itemDao;
  private LiveData<List<Item>> items;

  public ItemRepository( Application app ){
    AppDatabase db  = AppDatabase.getInstance( app );
    this.itemDao    = db.itemDao();
    items           = this.itemDao.getAll();
  }

  public void insertOne( Item item ){
    new InsertItemAsyncTask( itemDao ).execute( item );
  }

  public void updateOne( Item item ){
    new UpdateItemAsyncTask( itemDao ).execute( item );
  }

  public void deleteOne( Item item ){
    new DeleteItemAsyncTask( itemDao ).execute( item );
  }

  public void deleteAll(){
    new DeleteAllItemAsyncTask( itemDao ).execute();
  }

  public LiveData<List<Item>> getItems() {
    return items;
  }

  private static class InsertItemAsyncTask extends AsyncTask< Item, Void, Void >{
    private ItemDao itemDao;

    private InsertItemAsyncTask( ItemDao itemDao ){
      this.itemDao = itemDao;
    }

    @Override
    protected Void doInBackground(Item... items) {
      itemDao.insertOne( items[ 0 ]);
      return null;
    }
  }

  private static class UpdateItemAsyncTask extends AsyncTask< Item, Void, Void >{
    private ItemDao itemDao;

    private UpdateItemAsyncTask( ItemDao itemDao ){
      this.itemDao = itemDao;
    }

    @Override
    protected Void doInBackground(Item... items) {
      itemDao.updateOne( items[ 0 ]);
      return null;
    }
  }

  private static class DeleteItemAsyncTask extends AsyncTask< Item, Void, Void >{
    private ItemDao itemDao;

    private DeleteItemAsyncTask( ItemDao itemDao ){
      this.itemDao = itemDao;
    }

    @Override
    protected Void doInBackground(Item... items) {
      itemDao.delete( items[ 0 ]);
      return null;
    }
  }

  private static class DeleteAllItemAsyncTask extends AsyncTask< Void, Void, Void >{
    private ItemDao itemDao;

    private DeleteAllItemAsyncTask( ItemDao itemDao ){
      this.itemDao = itemDao;
    }

    @Override
    protected Void doInBackground(Void... voids) {
      itemDao.deleteAll();
      return null;
    }
  }
}
