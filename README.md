# Mobile App


## Getting started

To open the project using [Android Studio](https://developer.android.com/studio/) is recommended.


## Video and Learning diary

Video demonstrating functionality of the application and learning diary is available [here](https://drive.google.com/drive/folders/1PWkNOre8SoJNsnIIOak1L1sTvE22dFz5?usp=sharing).